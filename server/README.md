# How to run

1. Install node modules with command: `npm install`
2. Fill in file .env with your mongoDB database
3. Run server: `npm start`
4. Folder routes/user contains endpoints which you can test using software Postman
(Additionally: in folder postman are some request collections which you can import)