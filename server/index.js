const express = require('express');
const app = express();
const dotenv = require('dotenv'); //for values from file .env
const mongoose = require('mongoose');
const verify = require('./functions/user/verifyToken');

//import routes
const authRoute = require('./routes/user/auth');

dotenv.config();
const port = process.env.PORT || 3001;

//connect to DB
mongoose.connect(process.env.DB_CONNECT);
mongoose.connection.on('connected', () => {
    console.log('connected to mongo database');
});
 
mongoose.connection.on('error', err => {
    console.log('Error at mongoDB: ' + err);
});

//middleware
app.use(express.json()); // request object recognize as json object => req.body.name

//route middlewares
app.use('/api/user',authRoute);

app.listen(3001, () => console.log('Server up and running'));
