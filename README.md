## How to run

1. Instalirati node modules komandom: 'npm install'
2. Popuniti fajl .env sa vasom bazom (email podaci nisu neophodni za rad)
3. Pokrenuti server: 'npm start'
4. Folder routes/user sadrzi endpoints koje mozete gadjati pomocu Postmana
5. Dokumentacija swagger (radi za neke endpoint), se nalazi na adresi *http://localhost:3001/api-docs/*

### Primer registracije 


*  POST: http://localhost:3001/api/user/register

*  body JSON: `{
	"name":"Student",
	"email": "student13@gmail.com",
	"password": "1234567"
}`