const dotenv = require('dotenv');
const nodemailer = require('nodemailer');

const sendEmail = function(to,subject,html){
    var transporter = nodemailer.createTransport({
        service: process.env.MAIL_SERV,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS
        }
    }); 
    var mailOptions = {
        from: process.env.MAIL_USER,
        to: to,
        subject: subject,
        html: html
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
};        
module.exports = sendEmail;