const router = require('express').Router();
const User = require('../../model/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation,loginValidation} = require('../../functions/user/validation');

//api/user/

//REGISTER USER
//reqJSON: email,name,password
//resJSON: _id,name
router.post('/register', async (req,res) => {
    //validate data for user
    //probably will be moved to frontend
    //returns {error:{...details:[{message:...}]...},value:{...}}
    const {error} = registerValidation(req.body);
    if(error) 
        return res.status(400).send(error.details[0].message);
    
    //checking if user is alreay in DB
    const emailExist = await User.findOne({email: req.body.email});
    if(emailExist)
        return res.status(400).send('Email already exists');
    
    //hash password
    const salt = await bcrypt.genSalt(10); //complexity hash function
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    //create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });
    try{
        const savedUser = await user.save();
        res.send({userId: savedUser._id,userName: savedUser.name});
    }catch(err){
        res.status(400).send(err);
    }
});

//LOGIN USER
//reqJSON: email,password
//res: 'Loggen in!' + in header is JWT as 'auth-token'
router.post('/login', async (req,res) => {

    //validate data for user
    const {error} = loginValidation(req.body);
    if(error) 
        return res.status(400).send(error.details[0].message);

    //checking if user exists
    const user = await User.findOne({email: req.body.email});
    if(!user)
        return res.status(400).send('User is not found');
    
    //password check
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass)
        return res.status(400).send('Invalid password');

    //Create and assign token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send('Logged in!');     
});
module.exports = router;