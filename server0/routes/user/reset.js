const router = require('express').Router();
const User = require('../../model/User');
const bcrypt = require('bcryptjs');
const randomstring = require('randomstring');
const nodemailer = require('nodemailer');
const mailer = require('../../functions/user/mailer');
const dotenv = require('dotenv');

//api/user/reset

//GET USER INFO before reset, based on email
//query: email //.../reset?email=student@gmail.com
//resJSON: _id,name 
router.get('/', async (req,res) => {
    const user = await User.findOne({email: req.query.email});
    if(!user)
        return res.status(400).send('User is not found');
    res.send({_id: user._id,name: user.name});
});

//SEND EMAIL WITH SECRET TOKEN for verification
//reqJSON: email
//'Email was sent'
router.post('/',async (req,res) => {
    const user = await User.findOne({email: req.body.email});
    if(!user)
        return res.status(400).send('User is not found');
    //generate secret token
    const secretToken =randomstring.generate(7); //length of token
    try{
        const temp = await User.updateOne({email: req.body.email}, {secretToken: secretToken})
        const html = 
            `Hello there,
            <br/>
            Please verify your account by typing the following token:
            <br/>
            <b>${secretToken}</b>
            <br/>
            On the following page:
            <a href="http://lalallalalalala">http://lalalalalla</a>
            <br/>
            Have a nice day!`;
        
        const info = mailer( user.email, 'Please verify your email', html);
        res.send('Email was sent');
    }catch(err){
        res.status(400).send(err);
    }
});

//CHECK IF SECRET TOKEN IS VALID
//reqJSON: email,token
//'Valid token'
router.post('/token',async (req,res) => {
    const user = await User.findOne({email: req.body.email});
    if(!user)
        return res.status(400).send('User is not found');
    if(req.body.secretToken != user.secretToken)
        return res.status(401).send('Invalid token');
    res.send('Valid token');
});

//RESET PASSWORD
//reqJSON: email,token,password //(new password)
//'You changed password successfully'
router.post('/password',async (req,res) => {
    const user = await User.findOne({email: req.body.email});
    if(!user)
        return res.status(400).send('User is not found');
    //without check somebody could skip mail verification and do this
    if(req.body.secretToken != user.secretToken)
        return res.status(401).send('Invalid token');

    const samePass = await bcrypt.compare(req.body.password, user.password);
    if(samePass)
        return res.send('New password is same as old');
    else{
        const salt = await bcrypt.genSalt(10); //complexity hash func
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        try{
            const upd = await User.updateOne({_id: user._id}, {password: hashedPassword})
            res.send('You changed password successfully');
        }catch(err){
            res.status(400).send(err);
        }
    } 
});
module.exports = router;