const router = require('express').Router();
const verify = require('../../functions/user/verifyToken');
const User = require('../../model/User');
const bcrypt = require('bcryptjs');

//api/user/update
//**all API's are verified before with middleware function from verifyToken.js

//GET USER INFO before update, based on JWT token
//no parameter
//resJSON: _id,name,email
router.get('/', async (req,res) => {
    const user = await User.findOne({_id: req.user._id});
    if(!user)
        return res.status(400).send('User is not found');
    res.send({_id: user._id,name: user.name,email: user.email});
});

//CHANGE EMAIL
//reqJSON: email //(new email)
//'You changed email successfully'
router.post('/email',async (req,res) => {
    const user = await User.findOne({_id: req.user._id});
    if(!user)
        return res.status(400).send('User is not found');
    try{
        const upd = await User.updateOne({_id: user._id}, {email: req.body.email})
        res.send('You changed email successfully');
    }catch(err){
        res.status(400).send(err);
    }    
});

//CHANGE NAME
//reqJSON: name //(new name)
//'You changed name successfully'
router.post('/name', async (req,res) => {
    const user = await User.findOne({_id: req.user._id});
    if(!user)
        return res.status(400).send('User is not found');
    try{
        const upd = await User.updateOne({_id: user._id}, {name: req.body.name})
        res.send('You changed name successfully');
    }catch(err){
        res.status(400).send(err);
    }    
});

//CHANGE PASSWORD
//reqJSON: password //(new password)
//'You changed password successfully'
router.post('/password',async (req,res) => {
    const user = await User.findOne({_id: req.user._id});
    if(!user)
        return res.status(400).send('User is not found');
    const samePass = await bcrypt.compare(req.body.password, user.password);
    if(samePass)
        return res.send('New password is same as old');
    else{
        const salt = await bcrypt.genSalt(10); //complexity hash function
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        try{
            const upd = await User.updateOne({_id: user._id}, {password: hashedPassword})
            res.send('You changed password successfully');
        }catch(err){
            res.status(400).send(err);
        }
    } 
});
module.exports = router;