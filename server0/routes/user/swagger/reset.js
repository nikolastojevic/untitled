/** 
* @swagger
* tags:
* - name: "User password reset"
*   description: "Everything about password reset"
*paths:
*  /api/user/reset:
*    get:
*      tags:
*      - "User password reset"
*      summary: "Get user information with email"
*      description: "Check if email exists in case user forgot password"
*      produces:
*      - "application/json"
*      parameters:
*      - in: "query"
*        name: "email"
*        description: "Only user email"
*        required: true
*        schema:
*           type: string 
*           example: "student@gmail.com"
*      responses:
*       400:
*          description: "User is not found"
*       200:
*          schema:
*           type: "object"
*           required:
*           - "name"
*           - "_id"
*           properties:
*               _id:
*                   type: string
*                   example: "5e9628c846cf53163ce48586" 
*               name:
*                   type: string
*                   example: "Student"
*/