const express = require('express');
const app = express();
const dotenv = require('dotenv'); //for values from file .env
const mongoose = require('mongoose');
const verify = require('./functions/user/verifyToken');

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require ('swagger-ui-express');

//import routes
const authRoute = require('./routes/user/auth');
const updateRoute = require('./routes/user/update');
const resetRoute = require('./routes/user/reset');

dotenv.config();
const port = process.env.PORT || 3001;

//setup swagger documentation generator
const swaggerOptions = {
    swaggerDefinition:{
        info: {
            title: "Instant-CV",
            description: "This app creates CV for users, based on their information and template they choosed",
            contact: {
            name: "MATF Instant-CV Team"
            },
            version: "0.0.1"
        }    
    },
    apis: ['./routes/user/swagger/*.js']     
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',swaggerUi.serve,swaggerUi.setup(swaggerDocs));

//connect to DB
mongoose.connect(process.env.DB_CONNECT);
mongoose.connection.on('connected', () => {
    console.log('connected to mongo database');
});
 
mongoose.connection.on('error', err => {
    console.log('Error at mongoDB: ' + err);
});

//middleware
app.use(express.json()); // request object recognize as json object => req.body.name

//route middlewares
app.use('/api/user',authRoute);
//verify JWT token for api
app.use('/api/user/update',verify,updateRoute);
app.use('/api/user/reset',resetRoute);

app.listen(3001, () => console.log('Serverup and running'));
